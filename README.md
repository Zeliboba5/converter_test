# JSON template converter

Converts JSON file to HTML template

Runs on Python 3.6+

To run:

`python run.py <json_file_path>`

To run tests:

```
pip install -r requirements.txt
pytest
```

To run with docker:

```
docker build --tag=json-converter .
docker run -ti --rm --name=json-converter json-converter python run.py <json_file_path>
```
