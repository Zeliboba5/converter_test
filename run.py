import json
import sys
from collections import OrderedDict

from converter import process


def run():
    target_file = sys.argv[1] if len(sys.argv) > 1 else 'default_input.json'
    with open(target_file, 'r') as f:
        input_data = json.load(f, object_pairs_hook=OrderedDict)
    result = process(input_data)
    print(result)


if __name__ == '__main__':
    run()
