from collections import OrderedDict

from converter import process


def test_single_element():
    input_data = OrderedDict(
        h1='test',
        p='content',
    )
    result = process(input_data)
    assert result == '<h1>test</h1><p>content</p>'


def test_single_element_in_list():
    input_data = [OrderedDict(
        h1='test',
        p='content',
    )]
    result = process(input_data)
    assert result == '<ul><li><h1>test</h1><p>content</p></li></ul>'


def test_multi_element():
    input_data = [
        OrderedDict(
            h1='test',
            p='content',
        ),
        OrderedDict(
            h1='test1',
            p='content2',
        )
    ]
    result = process(input_data)
    assert result == '<ul><li><h1>test</h1><p>content</p></li><li><h1>test1</h1><p>content2</p></li></ul>'


def test_nested_object():
    input_data = [OrderedDict(
        h1='test',
        p='content',
        body=OrderedDict(
            h1='test',
            p='content',
        )
    )]
    result = process(input_data)
    assert result == '<ul><li><h1>test</h1><p>content</p><body><h1>test</h1><p>content</p></body></li></ul>'


def test_nested_list():
    input_data = [OrderedDict(
        h1='test',
        p='content',
        body=[OrderedDict(
            h1='test',
            p='content',
        )],
    )]
    result = process(input_data)
    assert result == ('<ul><li>'
                      '<h1>test</h1><p>content</p>'
                      '<body><ul><li><h1>test</h1><p>content</p></li></ul></body>'
                      '</li></ul>')


def test_nested_object_depth():
    input_data = [OrderedDict(
        h1='test',
        p='content',
        body=OrderedDict(
            h1='test',
            p='content',
            body=OrderedDict(
                h1='test',
                p='content',
            )
        )
    )]
    result = process(input_data)
    assert result == ('<ul><li>'
                      '<h1>test</h1><p>content</p>'
                      '<body>'
                      '<h1>test</h1><p>content</p><body><h1>test</h1><p>content</p></body>'
                      '</body>'
                      '</li></ul>')


def test_element_class():
    input_data = OrderedDict(**{
        'h1.class-1': 'test',
        'p': 'content',
    })
    result = process(input_data)
    assert result == '<h1 class="class-1">test</h1><p>content</p>'


def test_element_multi_class():
    input_data = OrderedDict(**{
        'h1.class-1.class-2': 'test',
        'p': 'content',
    })
    result = process(input_data)
    assert result == '<h1 class="class-1 class-2">test</h1><p>content</p>'


def test_element_with_id():
    input_data = OrderedDict(**{
        'h1#my-id': 'test',
        'p': 'content',
    })
    result = process(input_data)
    assert result == '<h1 id="my-id">test</h1><p>content</p>'


def test_element_multi_class_with_id():
    input_data = OrderedDict(**{
        'h1.class-1.class-2#my-id': 'test',
        'p': 'content',
    })
    result = process(input_data)
    assert result == '<h1 id="my-id" class="class-1 class-2">test</h1><p>content</p>'


def test_value_escape():
    input_data = OrderedDict(
        h1='test',
        p='content<script> very evil script </script>',
    )
    result = process(input_data)
    assert result == '<h1>test</h1><p>content&lt;script&gt; very evil script &lt;/script&gt;</p>'


def test_multiply_ids():
    input_data = OrderedDict(**{
        'h1.class-1.class-2#my-id#test': 'test',
        'p': 'content',
    })
    result = process(input_data)
    assert result == '<h1 id="my-id test" class="class-1 class-2">test</h1><p>content</p>'


def test_multiply_ids_mixed():
    input_data = OrderedDict(**{
        'h1#id1.class-1#my-id.class-2#test': 'test',
        'p': 'content',
    })
    result = process(input_data)
    assert result == '<h1 id="id1 my-id test" class="class-1 class-2">test</h1><p>content</p>'
