import html
from collections import OrderedDict, Sequence, Mapping
from typing import List, Union


ELEMENT_TEMPLATE = '{opening_tag}{value}{closing_tag}'


def _process_tag(tag_string):
    classes = []
    ids = []
    tag_string_id_split = tag_string.split('#')
    tag_string_class_split = tag_string_id_split[0].split('.')
    tag = tag_string_class_split[0]
    classes += tag_string_class_split[1:]

    for element_id_split in tag_string_id_split[1:]:
        element_id_split_by_class = element_id_split.split('.')
        ids.append(element_id_split_by_class[0])
        classes += element_id_split_by_class[1:]

    if classes and len(tag_string_id_split) > 1:
        return f'<{tag} id="{" ".join(ids)}" class="{" ".join(classes)}">', f'</{tag}>'
    elif classes:
        return f'<{tag} class="{" ".join(classes)}">', f'</{tag}>'
    elif len(tag_string_id_split) > 1:
        return f'<{tag} id="{" ".join(ids)}">', f'</{tag}>'
    else:
        return f'<{tag}>', f'</{tag}>'


def _process_item(item: OrderedDict):
    result_string = ''
    for element, value in item.items():
        opening_tag, closing_tag = _process_tag(element)
        if isinstance(value, Sequence) and not isinstance(value, str):
            string_value = _process_list(value)
        elif isinstance(value, Mapping):
            string_value = _process_item(value)
        else:
            string_value = html.escape(value)
        result_string += ELEMENT_TEMPLATE.format(
            opening_tag=opening_tag,
            closing_tag=closing_tag,
            value=string_value,
        )
    return result_string


def _process_list(input_data: List[OrderedDict]):
    result_string = ''
    for item in input_data:
        result_string += f'<li>{_process_item(item)}</li>'
    return f'<ul>{result_string}</ul>'


def process(input_data: Union[List[OrderedDict], OrderedDict]):
    if isinstance(input_data, Sequence):
        return _process_list(input_data)
    elif isinstance(input_data, Mapping):
        return _process_item(input_data)
    else:
        raise ValueError('Unknown input type format')
